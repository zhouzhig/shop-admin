/*
 * @Description: 
 * @Version: 1.0
 * @Autor: 周周
 * @Date: 2023-03-28 14:06:40
 * @LastEditors: 周周
 * @LastEditTime: 2023-03-28 14:08:49
 */
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  devServer:{
    host:'localhost',
    port:8000,
    open:true
  }
})
