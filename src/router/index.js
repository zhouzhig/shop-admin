import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '../views/login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: login
  },
  {
    path: '/bg',
    name: 'bg',
    component: () => import( '../views/bgView.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
